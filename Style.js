import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cameraContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    confirmBarCode: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
    barCodeText: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        top: 0,
        textAlign: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    }
})