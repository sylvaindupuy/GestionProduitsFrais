import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import { Icon } from 'native-base';

import About from './components/About';
import Home from './components/Home';

export default createDrawerNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      // set up icon here because Home is an stack navigation
      drawerIcon: () => {
        return <Icon name="md-home" size={24} />
      }
    }
  },
  About: { screen: About }
}, {
    initialRouteName: 'Home',
  }
)