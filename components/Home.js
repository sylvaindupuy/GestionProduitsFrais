import React, { Component } from 'react';
import { StackNavigator } from "react-navigation";
import { Container, Header, Content, Title, Text, Button, Left, Right, Body, Icon } from 'native-base';

import BarCodeReader from './BarCodeReader';
import style from '../Style';

class Home extends Component {

    static navigationOptions = ({ navigation }) => ({
        header: (
            <Header>
                <Left>
                    <Button transparent>
                        <Icon name='menu' onPress={() => navigation.openDrawer()} />
                    </Button>
                </Left>
                <Body>
                    <Title>Welcome</Title>
                </Body>
                <Right />
            </Header>
        )
    })

    render() {
        return (
            <Container>
                <Content contentContainerStyle={style.container}>
                    <Button light
                        onPress={() => this.props.navigation.navigate("BarCodeReader")}>
                        <Text> Scanner </Text>
                    </Button>
                </Content>
            </Container>
        );
    }

}

export default StackNavigator({
    Home: { screen: Home },
    BarCodeReader: { screen: BarCodeReader },
});;