import React, { Component } from 'react';
import { Header, Content, Container, Title, Button, Text, Left, Right, Body, Icon } from 'native-base';
import Camera from 'react-native-camera';

import style from '../Style';

export default class BarCodeReader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            barCodeType: '',
            barCodedata: '',
            showConfirmation: false
        }
    }

    static navigationOptions = ({ navigation }) => ({
        drawerIcon: () => {
            return <Icon name="md-home" size={24} />
        },
        header: (
            <Header>
                <Left>
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <Title>Scannez le code</Title>
                </Body>
                <Right />
            </Header>
        )
    })

    render() {
        return (
            <Container>
                <Content contentContainerStyle={style.cameraContainer}>
                    <Camera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={style.preview}
                        type={Camera.constants.Type.back}
                        flashMode={Camera.constants.FlashMode.auto}
                        onBarCodeRead={(e) => this.onBarCodeRead(e)}
                    />
                    {this.renderBarCodeData()}
                    {this.renderConfirmation()}
                </Content>
            </Container>
        );
    }

    renderBarCodeData() {
        if (this.state.showConfirmation) {
            this.camera.stopPreview();
            return (
                <Text style={style.barCodeText}> {this.state.barCodedata} </Text>
            );
        }
    }

    renderConfirmation() {
        if (this.state.showConfirmation) {
            this.camera.stopPreview();
            return (
                <Content style={style.confirmBarCode}>
                    <Button rounded>
                        <Text>Confirmer</Text>
                    </Button>
                    <Button rounded icon danger onPress={() => this.resetState()}>
                        <Icon name='trash' />
                    </Button>
                </Content>
            );
        }
    }

    resetState() {
        this.setState({ barCodeType: '', barCodedata: '', showConfirmation: false });
        this.camera.startPreview();
    }

    onBarCodeRead(e) {
        if (!this.state.showConfirmation) {
            this.setState({ barCodeType: e.type, barCodedata: e.data, showConfirmation: true });
        }
    }
}