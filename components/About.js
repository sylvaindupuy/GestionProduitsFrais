import React, { Component } from 'react';
import { Container, Header, Content, Title, Text, Button, Left, Right, Body, Icon } from 'native-base';

import style from '../Style';

class BarCodeReader extends Component {

    static navigationOptions = {
        drawerIcon: () => {
            return <Icon name="md-information-circle" size={24} />
        }
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='menu' onPress={() => this.props.navigation.openDrawer()} />
                        </Button>
                    </Left>
                    <Body>
                        <Title>About</Title>
                    </Body>
                    <Right />
                </Header>
                <Content contentContainerStyle={style.container}>
                    <Text> A propos de cette application </Text>
                </Content>
            </Container>
        );
    }

}

export default BarCodeReader;